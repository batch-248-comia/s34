//express package was imported as express
const express = require("express");

//invoked express package to create a server/api and save it in a variable which we can refer to later to create routes
const app = express();

//variable for port assignment
const port = 4000;

//express.json() is a method from express that allow us to handle the stream of data from our client and receive the data and automatically parse the incoming JSON from our request

//app.use() is a method used to run another function or method for our expressjs api
//it is used to run middlewares

app.use(express.json())

//by applying the option of "extended:true" this allows us to receive information in other data types such as an object which we will use throughout our application
app.use(express.urlencoded({extended:true}))

//CREATE A ROUTE IN EXPRESS
	//express has methods corresponding to each HTTP method

app.get("/",(req,res)=>{
	//res.send uses the express JS Module's method instead to send a response back to the client
	res.send("Hello from our default ExpressJs GET route!")
})

/*
app.post("/",(req,res)=>{
	res.send("Hello from our default ExpressJs POST route!")
})
*/


//MiniActivity
app.post("/",(req,res)=>{
	res.send(`Hello ${req.body.firstName} ${req.body.lastName}!`)
})

app.put("/",(req,res)=>{
	res.send("Hello from our default ExpressJs PUT route!")
})

app.delete("/",(req,res)=>{
	res.send("Hello from our default ExpressJs DELETE route!")
})

//ADD A MOCK DATABASE

let users = [

		{
			username: "cardo_dalisay",
			password: "quiapo"
		},
		{
			username: "mommy_d",
			password: "nardadarna"
		},
		{
			username: "kagawad_godbless",
			password: "dingangbato"
		}
	]

/*
app.post("/signup",(req,res)=>{
	//request/req.body contains the body of the request or the input
	users.push(req.body)
	res.send(users)
})

*/

//MA3
//Refactor


app.post("/signup",(req,res)=>{

	console.log(req.body);

	if(req.body.username !== "" && req.body.password !== "") { 
		users.push(req.body)
		res.send(`Users ${req.body.username} successfully registered!`)
	}
	else {
		res.send("Please input BOTH username and password.")
	}
	
})



/* 
app.post("/register",(req,res)=>{

	console.log(req.body);

	if(req.body.username && req.body.password !== null) { 
		users.push(req.body)
		res.send(users)
	}
	else {
		res.send("Error")
	}
	
})
*/

app.put("/change-password",(req,res)=>{

	//(1) creates a variable to store the message to be sent back to Postman
	let message;

	//creates a for loop that will loop through the elements of the "users" array
	for(let i=0; i<users.length; i++){

		if (req.body.username == users[i].username){

			users[i].password = req.body.password;

			message = `User ${req.body.username}'s password has been updated!`
			break;
		}else{
			message = `User does not exist!`
		}

	}

	res.send(message);
})


//ACTIVITY


app.get("/home",(req,res)=>{
	//res.send uses the express JS Module's method instead to send a response back to the client
	res.send("Welcome to the homepage!")
})

app.get("/users",(req,res)=>{
	//res.send uses the express JS Module's method instead to send a response back to the client
	res.send(users)
})

app.delete("/delete-user",(req,res)=>{

	//(1) creates a variable to store the message to be sent back to Postman
	let message;

	//creates a for loop that will loop through the elements of the "users" array
	for(let i=0; i<users.length; i++){

		if (req.body.username == users[i].username){

			users.splice(i,1);

			message = `User ${req.body.username} has been deleted!`
			break;
		}else{
			message = `User does not exist!`
		}

	}

	res.send(message);
})

//tells our server to listen to the port
app.listen(port,()=>console.log(`Server running at port ${port}`))

